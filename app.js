import van from 'vanjs-core'
import About from './pages/about.js'
import { Route, routeTo } from 'vanjs-router'
import List from './pages/list.js'

const { button, div, p, img } = van.tags

const en = {
    homePage: 'Page Home.',
    hello: 'hello',
    goToAbout: 'Go To About',
    goToHotList: 'Go To Hot List'
};


const th = {
    homePage: 'หน้าหลัก',
    hello: 'สวัสดี',
    goToAbout: 'ไปยังเกี่ยวกับ',
    goToHotList: 'ไปยังรายการยอดนิยม'
};


let currentLanguage = 'th'


function setLanguage(language) {
    currentLanguage = language;
    renderApp();
}
  

function getText(key) {
    return currentLanguage === 'th' ? th[key] : en[key];
}


const App = () => {

    return div(

        Route({ name: 'home' },

            p(getText('homePage'), div({ id: "headlinner" }, getText('hello'))),

            button({ onclick: () => routeTo('About'), class: "button" }, getText('goToAbout')),

            button({ onclick: () => routeTo('List', ['hot', '15']), class: "button" }, getText('goToHotList')),

            button({ onclick: () => {
                
                if (currentLanguage === 'th') {
                    setLanguage('en');
                } else {
                    setLanguage('th');
                }
            }
            , class: "button" }, ('Change Lang')),

        ),
        
       
        

        About(),
        List()

    )

}


van.add(document.body, App())

function renderApp() {
    van.clear(document.body);
    van.add(document.body, App());
}