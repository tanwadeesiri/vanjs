// List.js
import { Route, routeTo } from 'vanjs-router';
import van from 'vanjs-core';

const { button, div, p, img } = van.tags

const List = () => {

    const listType = van.state('');
    const listId = van.state('');
    const welcome = van.state('Welcome!');

    return Route({

        name: 'List',

        onFirst() {
            welcome.val = 'Welcome!';
        },

        onLoad(route) {
            const [type, id] = route.args;
            listType.val = type;
            listId.val = id;
        }

    },

    div(

        div({ id: "btn-left" },
            button({ onclick: () => routeTo('home'), class: "button" }, 'Back To Home'), ' '
        ),

        welcome,
        div('List Type: ', listType),
        div('List Id: ', listId),

    ));
    
};

export default List;
