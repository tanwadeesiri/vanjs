// About.js
import van from 'vanjs-core'
import { Route, routeTo } from 'vanjs-router'
import '../mysass.scss'


const { button, div, p, img } = van.tags

const About = () => {
    return Route({ name: 'About' },
        div(
            button({ onclick: () => routeTo('home'), class: "button"}, 'Back To Home'),
            'this is testing for about page dont look it much .',
            div(
                img({
                    style: 'position: relative; width: 1000px; height: 600px',
                    class: 'circle', src: 'https://images.nationalgeographic.org/image/upload/v1638892272/EducationHub/photos/hoh-river-valley.jpg' }),
                div({ 
                    style: 'font-size: 50px;background-color: rgba(255, 0, 125, 0.7); padding: 15px',
                    class: 'text-block' }, 'this is a forest image')
            )
        )
    )
}

export default About;
